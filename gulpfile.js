var gulp = require('gulp');
var uglify = require('gulp-uglify');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var notify = require("gulp-notify");
var coffee = require('gulp-coffee');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var gcmq = require('gulp-group-css-media-queries');
const babel = require('gulp-babel');

var paths = {
    project: 'PilDuyna/**/*',
    config: 'config/**/*',
    build: 'DOCKER_COMPOSE/web/PilDuyna/PilDuyna',
    scripts: 'SRC/COFFEE/*.coffee',
    scss: 'SRC/SCSS/**/*.scss',
    css: 'PilDuyna/static/css'};

function errorhandler() {
    var args = Array.prototype.slice.call(arguments);
    notify.onError({
       title: "Compile Error",
       message: "<%= error %>"
   }).apply(this, args);
    this.emit('end');
}


gulp.task('build', function () {
    gulp.src(paths.project).pipe(gulp.dest(paths.build));
    gulp.src(paths.config).pipe(gulp.dest(paths.build))
    .pipe(notify(({message:'Build copy'})))
});

gulp.task('scripts', function() {
		gulp.src(paths.scripts)
    .pipe(coffee({bare: true}).on('error', errorhandler))
    .pipe(concat('main.js')).on('error', errorhandler)
		.pipe(babel({
				presets: ['es2015']
			})).on('error', errorhandler)
		// .pipe(uglify()).on('error', errorhandler)
		.pipe(gulp.dest('PilDuyna/static/script'))
		.pipe(notify({ message: 'SCRIPT minifyand coffee make Complete' }))
});

gulp.task('sass', function() {
  gulp.src(paths.scss)
    .pipe(sass().on('error', errorhandler))
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ])).on('error', errorhandler)
    .pipe(gcmq())
    .pipe(gulp.dest(paths.css))
		.pipe(notify(({message:'Sass to css compile succesful'})))
});

gulp.task('default', function() {
		gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.scss, ['sass'])
});
