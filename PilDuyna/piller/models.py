from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):              # __unicode__ on Python 2
        return self.name


class Quote(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    text = models.TextField(blank=False)

    def __str__(self):
        return self.text