from .models import Quote
from multiprocessing import Pool
from django.http import HttpResponse, HttpResponseBadRequest
from django.core.paginator import Paginator, InvalidPage
from django.views.decorators.csrf import csrf_protect
from django.template import loader
from django.conf import settings
FONTS_PATH = settings.FONTS
from ..utils.quote_generate import QuterCreater


FONTS= {'love':'FallingSkyExt.otf', 'casual':'odstemplik.otf', 'destiny':'Digitalt.otf'}
quote = []
@csrf_protect
def index(request):
    global quote
    pool = Pool()
    if quote == []:
        quotes = Quote.objects.all()
        quote = [(quote.text, quote.author.name,) for quote in quotes]
    FRAME_SIZE = 500
    FONT_IN = FONTS_PATH / FONTS['casual']
    template = loader.get_template('index.html')
    paginator = Paginator(quote, 20)
    if request.method == "POST":
        if request.is_ajax():
            if request.POST.get('page_number'):
                page_number = int(request.POST.get('page_number'))
                try:
                    page_objects = paginator.page(page_number).object_list
                except InvalidPage:
                    return HttpResponse({"":""}, status=200)
                except UnboundLocalError:
                    return HttpResponseBadRequest({"":""}, status=440)

                multiple_results = pool.map(
                    QuterCreater(FONT_IN, FRAME_SIZE).run,page_objects)
                resu = ["data:image/png;base64," +
                        res.decode('utf-8') + '|||'
                        for res in multiple_results]
                return HttpResponse(resu, status=200)

    page_objects = paginator.page(1).object_list
    multiple_results = pool.map(QuterCreater(FONT_IN, FRAME_SIZE).run, page_objects)
    context = {
                'images': ["data:image/png;base64," + res.decode('utf-8') for res in multiple_results]
            }
    return HttpResponse(template.render(context), status=200)


