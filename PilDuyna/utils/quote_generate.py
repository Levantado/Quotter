"""
    Module that take quote, author of quote an size of pictures and
    draw Pictures
"""
from multiprocessing import Pool
import base64
import asyncio
from io import BytesIO
import logging
import datetime

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

logging.basicConfig(format='%(levelname)s : %(asctime)s : %(message)s',
                    level=logging.ERROR)


class QuterCreater:
    """
        QuterCreater complex class, that take on input fonts, text, and author
        of the text and formating post card with size from input
    """
    def __init__(self, font, size):
        self.quote = dict()
        self.quote['font_quote'] = font
        self.quote['size_frame'] = size

        self.quote['img'] = Image.new(
            'RGB',
            (size, size),
            (255, 255, 255))

    @staticmethod
    def checker(font_import, list_words, limit_width, first_string=None):
        """
            Static method that calculate spaces for fill of limit size of
            string with space after words
        :param font_import: fonts that use for calculate width
        :param list_words: list of words that we convert in text with space
        :param limit_width: limit of size string
        :param first_string: flag for add white space before string
        :return:
        """
        start = datetime.datetime.now()
        logging.info('Enter in checker')
        if first_string == 'Yes':
            list_words.insert(0, '    ')
        text_test = []
        width = 200
        spacer = [1 for _ in list_words[:-1]]
        output_text = ''
        while width < limit_width:
            for key, word in enumerate(list_words[:-1]):
                if key == len(list_words) - 2:
                    if spacer[key] > min(spacer):
                        pass
                    else:
                        spacer[key] += 1
                else:
                    if spacer[key] < spacer[key+1]:
                        spacer[key] += 1
                text_test.append(word+(spacer[key]*' '))
            text_test.append(list_words[-1])
            width, _ = font_import.getsize(''.join(text_test))
            output_text = ''.join(text_test)
            text_test = []
        logging.info(f'Exit from checker: {datetime.datetime.now()-start}')
        return output_text

    def creater_base_layout(self):
        """
            Method that prepare layot for post text, it's create grey
            rectangle with white border with size that fix in init
        """
        start = datetime.datetime.now()
        logging.info('Enter in creater_base_layout')
        self.quote['draw'] = ImageDraw.Draw(self.quote['img'])
        self.quote['draw'].rectangle(
            (10, self.quote['img'].size[1] - 10,
             self.quote['img'].size[0] - 10, 10),
            fill="#F0F0F0")
        logging.info(f'Exit from creater_base_layout: '
                     f'{datetime.datetime.now()-start}')
        return

    def size_quote_detector(self):
        """
        Method detect width and hight of quote and change font-size that qoute
        fill rectangle size of frame - 150 and size of frame - 100
        """
        start = datetime.datetime.now()
        logging.info('Enter in size_quote_detector')
        frame_max = self.quote['size_frame'] - 150
        frame_min = frame_max - 50
        f_size_max = 100
        f_size_min = 1
        f_size = 28
        f_size_prev = 0
        while True:
            font = ImageFont.truetype(str(self.quote['font_quote']), f_size)
            len_string, f_h = font.getsize(self.quote['quote_text'] + '    ')
            count_string = len_string // (self.quote['size_frame'] - 100) + (
                len_string % (self.quote['size_frame'] - 100) > 0)
            h_quoytes = count_string * (f_h * 2)
            if h_quoytes > frame_max:
                f_size_max = f_size
            else:
                f_size_min = f_size
            if frame_max > h_quoytes > frame_min:
                break
            f_size = (f_size_max+f_size_min)//2
            if f_size_prev == f_size:
                break
            f_size_prev = f_size
            logging.info(f'FOnt size: {f_size}')
            logging.info(f'Vertical size: {h_quoytes}')
            logging.info(f'Frame max: {frame_max}')
            logging.info(f'Frame min: {frame_min}')
            logging.info(f'F_size max: {f_size_max}')
            logging.info(f'F_size min: {f_size_min}')
        self.quote['font_size'] = f_size
        self.quote['fonts'] = ImageFont.truetype(
            str(self.quote['font_quote']),
            self.quote['font_size'])
        self.quote['len_string'], self.quote['f_h'] = self.quote['fonts'].\
            getsize(self.quote['quote_text'] + '    ')
        self.quote['count_string'] = (self.quote['len_string'] //
                                      (self.quote['size_frame'] - 100) +
                                      (self.quote['len_string'] %
                                       (self.quote['size_frame'] - 100) > 0))
        self.quote['heigh_quoytes'] = (self.quote['count_string'] *
                                       (self.quote['f_h'] * 1.5))
        self.quote['start_point_x'] = 50
        self.quote['start_point_y'] = int(
            ((self.quote['size_frame'] - 150) -
             self.quote['heigh_quoytes']) / 2) + 50
        logging.info(f'Exit from size_quote_detector:'
                     f' {datetime.datetime.now()-start}')

        return

    def generator_text_layout(self):
        """
        Method separete quote on the line, with line limits and then use
         static methkd for fill spaces.

        """
        start = datetime.datetime.now()
        logging.info('Enter in generator_text_layout')
        words = self.quote['quote_text'].split()
        key = 0
        texts = []
        flag = 0
        for count_st in range(0, self.quote['count_string']+1):
            text_output = ''
            while True:
                try:
                    if texts == [] and text_output == '':
                        text_output += '    ' + words[key] + ' '
                    else:
                        text_output += words[key] + ' '
                except IndexError:
                    break
                text_widht, _ = self.quote['fonts'].getsize(text_output)
                if text_widht > (self.quote['size_frame'] - 100):
                    first = None
                    if count_st == 0:
                        first = 'Yes'
                    text_output = text_output[:-(len(words[key])+1)]
                    # texts.append(text_output)
                    texts.append(
                         self.checker(
                            self.quote['fonts'],
                            text_output.split(),
                            self.quote['size_frame'] - 100,
                            first))
                    text_output = ''
                    flag = 1
                    break
                key += 1
            if flag == 1:
                flag = 0
            else:
                if text_output != "":
                    texts.append(text_output)
            self.quote['texts'] = texts
        logging.info(f'Exit from generator_text_layout:'
                     f' {datetime.datetime.now()-start}')

        return

    def text_placer(self):
        """
        Draw text on base layout
        """
        start = datetime.datetime.now()
        logging.info('Enter in text_placer')
        for key, text_string in enumerate(self.quote['texts']):
            y_position = self.quote['start_point_y'] + \
                         (self.quote['f_h'] * 1.5 * key)
            self.quote['draw'].text(
                (self.quote['start_point_x'], y_position),
                text_string,
                (49, 49, 49),
                font=self.quote['fonts'])
        logging.info(f'Exit from text_placer: {datetime.datetime.now()-start}')

    def author_placer(self):
        """
        Method draw Author of quotes before calculate optimal size with limit
        """
        start = datetime.datetime.now()
        logging.info('Enter in author_placer')
        f_size = int(self.quote['font_size'] * 0.9)
        fonts = ImageFont.truetype(str(self.quote['font_quote']), f_size)
        author_width, author_hight = fonts.getsize(self.quote['author'])
        author_start_x = ((self.quote['size_frame'] * 0.8) - author_width) + 75
        author_start_y = self.quote['size_frame'] * 0.85 - author_hight
        self.quote['draw'].text(
            (author_start_x, author_start_y),
            self.quote['author'],
            (49, 49, 49),
            fonts)
        logging.info(f'Exit from author_placer:'
                     f' {datetime.datetime.now()-start}')

    def saver(self):
        """
        return image like base64
        """
        start = datetime.datetime.now()
        logging.info('Enter in saver')
        bytes_io = BytesIO()
        self.quote['img'].save(bytes_io, 'PNG', optimize=True)
        logging.info(f'Exit from saver: {datetime.datetime.now()-start}')
        result = base64.b64encode(bytes_io.getvalue())
        return result

    def run(self, quote):
        """
        method that run all methods
        """
        start = datetime.datetime.now()
        logging.info('Enter in run')
        # loop = asyncio.new_event_loop()
        self.quote['quote_text'] = quote[0]
        self.quote['author'] = quote[1]
        self.size_quote_detector()
        self.generator_text_layout()
        self.creater_base_layout()
        self.text_placer()
        self.author_placer()
        # task = asyncio.wait(
        #     [loop.create_task(self.text_placer()),
        #      loop.create_task(self.author_placer()),
        #      ])
        # loop.run_until_complete(task)
        # loop.stop()
        # loop.close()
        x = self.saver()
        logging.info(f'Exit from run: {datetime.datetime.now()-start}')
        return x

if __name__ == "__main__":

    from pathlib import Path

    START_TEST = datetime.datetime.now()
    PATH_APP = Path(__file__).parent.parent
    PATH_IN = PATH_APP / 'static' / 'fonts'
    PATH_OUT = PATH_APP / 'output'
    FONTS_ARRAY = ['FallingSkyExt.otf',
                   'odstemplik.otf',
                   'Digitalt.otf',
                   'Montserrat-Regular.ttf',
                   'Monument_Valley_1.2-Regular.otf']
    FONT_IN = PATH_IN / FONTS_ARRAY[1]
    FRAME_SIZE = 500
    HEIGH_QUOTES = 0
    FONT_SIZE = 0
    TEXT = "The opposite of love is not hate, it's indifference. The " \
           "opposite of art is not ugliness, it's indifference. The " \

    AUTHOR = 'Elie Wiesel'
    POOL = Pool()
    POOL.daemon = False
    QUOTE_INPUT = [(TEXT, AUTHOR, ) for _ in range(20)]
    RESULTS = POOL.map(QuterCreater(FONT_IN, FRAME_SIZE).run, QUOTE_INPUT)
    POOL.close()
    POOL.join()
    print(datetime.datetime.now() - START_TEST)
