'use strict';

var body, count, creator, hasNextPage, img, loadItems, _loadOnScroll, over, pageNum, pager, perTick, _scrollTo, toggler, xhr;

document.addEventListener('DOMContentLoaded', function () {
  return window.addEventListener('scroll', _loadOnScroll, false);
});

xhr = new XMLHttpRequest();

creator = function creator(a, b) {
  var frame, img, ul;
  ul = document.getElementById("shelf");
  frame = document.createElement("li");
  img = document.createElement("img");
  frame.setAttribute('class', 'frame');
  img.setAttribute('class', 'imager small_size');
  img.setAttribute('id', a);
  img.setAttribute('src', b);
  ul.appendChild(frame);
  frame.appendChild(img);
};

perTick = 0;

pageNum = 1;

count = 20;

hasNextPage = true;

_loadOnScroll = function loadOnScroll() {
  if (document.body.scrollTop > document.body.scrollHeight - document.body.clientHeight * 2) {
    window.removeEventListener('scroll', _loadOnScroll, false);
    loadItems();
  }
};

document.getElementById('Topper').onclick = function () {
  _scrollTo(document.body, 0, 100);
};

_scrollTo = function scrollTo(element, to, duration) {
  var difference, run;
  if (duration < 0) {
    return;
  }
  difference = to - element.scrollTop;
  perTick = difference / duration * 2;
  run = function run() {
    element.scrollTop = element.scrollTop + perTick;
    return _scrollTo(element, to, duration - 2);
  };
  return setTimeout(run, 10);
};

pager = document.getElementById('Pager');

img = document.getElementsByTagName("img");

body = document.body;

over = document.getElementById('over');

document.addEventListener("click", function (e) {
  if (e.target.tagName === 'IMG') {
    toggler(body, 'stop-scrolling');
    toggler(over, 'overlay');
    toggler(over, 'overoff');
    toggler(e.target, 'small_size');
    toggler(e.target, 'real_size');
  }
});

toggler = function toggler(el, cls) {
  var cls_name, start_index;
  if (el.className) {
    if (el.className.indexOf(cls) !== -1) {
      start_index = el.className.match(cls).index;
      cls_name = el.className;
      return el.className = cls_name.substr(0, start_index) + cls_name.substr(start_index + cls.length);
    } else {
      return el.className += ' ' + cls;
    }
  }
};

loadItems = function loadItems() {
  var a, getCookie, listener;
  if (hasNextPage === false) {
    return false;
  }
  pageNum += 1;
  count += 20;
  getCookie = function getCookie(c_name) {
    var c_end, c_start;
    if (document.cookie.length > 0) {
      c_start = document.cookie.indexOf(c_name + "=");
      if (c_start !== -1) {
        c_start = c_start + c_name.length + 1;
        c_end = document.cookie.indexOf(";", c_start);
        if (c_end === -1) {
          c_end = document.cookie.length;
        }
        return unescape(document.cookie.substring(c_start, c_end));
      }
    }
    return "";
  };
  listener = function listener() {
    var a, data, i, j, ref;
    if (xhr.status === 200) {
      hasNextPage = true;
      data = xhr.responseText;
      if (data === "") {
        return;
      }
      a = data.split('|||');
      a.pop();
      for (i = j = 0, ref = a.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
        creator('ImageLink' + (count + i + 1), a[i]);
      }
      window.addEventListener('scroll', _loadOnScroll, false);
      pager.innerHTML = "Page number:" + pageNum;
    }
  };
  xhr.open('POST', '', true);
  xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
  xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  a = "page_number=" + pageNum;
  xhr.send(a);
  xhr.onload = function () {
    return listener();
  };
  xhr.onerror = function (e) {
    return console.log(e);
  };
};