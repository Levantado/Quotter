  document.addEventListener('DOMContentLoaded', ()->
    window.addEventListener('scroll', loadOnScroll, false)
  )

  xhr = new XMLHttpRequest()
  creator = (a,b)->
    ul = document.getElementById("shelf")
    frame = document.createElement("li")
    img = document.createElement("img")
    frame.setAttribute('class', 'frame')
    img.setAttribute('class', 'imager small_size')
    img.setAttribute('id', a)
    img.setAttribute('src', b)
    ul.appendChild(frame)
    frame.appendChild(img)
    return

  perTick = 0
  pageNum = 1
  count = 20
  hasNextPage = true;
  loadOnScroll = () ->
    if document.body.scrollTop > document.body.scrollHeight - (document.body.clientHeight*2)
      window.removeEventListener('scroll', loadOnScroll, false)
      loadItems()
    return

  document.getElementById('Topper').onclick = () ->
    scrollTo(document.body, 0, 100)
    return

  scrollTo = (element, to, duration) ->
    if (duration < 0)
      return
    difference = to - element.scrollTop;
    perTick = difference / duration * 2;

    run = () ->
      element.scrollTop = element.scrollTop + perTick
      scrollTo(element, to, duration - 2)

    setTimeout(run, 10)

  pager = document.getElementById('Pager')
  img = document.getElementsByTagName("img")
  body = document.body
  over = document.getElementById('over')

  document.addEventListener("click", (e) ->
    if (e.target.tagName == 'IMG')
      toggler(body, 'stop-scrolling')
      toggler(over, 'overlay')
      toggler(over, 'overoff')
      toggler(e.target, 'small_size')
      toggler(e.target, 'real_size')
    return
  )

  toggler = (el, cls) ->
    if el.className
      if el.className.indexOf(cls) != -1
        start_index = el.className.match(cls).index
        cls_name = el.className
        el.className = cls_name.substr(0,start_index)+cls_name.substr(start_index+cls.length)
      else
        el.className += ' '+cls

  loadItems = () ->
    if (hasNextPage == false)
      return false
    pageNum +=  1
    count += 20
    getCookie = (c_name) ->
      if (document.cookie.length > 0)
        c_start = document.cookie.indexOf(c_name + "=")
        if (c_start != -1)
            c_start = c_start + c_name.length + 1
            c_end = document.cookie.indexOf(";", c_start)
            if (c_end == -1)
              c_end = document.cookie.length
            return unescape(document.cookie.substring(c_start,c_end))
      return ""

    listener = ->
      if xhr.status == 200
        hasNextPage = true;
        data = xhr.responseText
        if data is ""
          return
        a = data.split('|||')
        a.pop()
        creator 'ImageLink'+(count+i+1),a[i] for i in [0..a.length-1]
        window.addEventListener('scroll', loadOnScroll, false)
        pager.innerHTML = "Page number:" + pageNum
      return

    xhr.open('POST', '', true)
    xhr.setRequestHeader("X-CSRFToken",getCookie("csrftoken"))
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest")
    a = "page_number="+pageNum
    xhr.send(a)
    xhr.onload = -> listener()
    xhr.onerror = (e) -> console.log(e)
    return


